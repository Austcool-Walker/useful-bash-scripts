#!/bin/bash
echo '==> Building Minecraft Bedrock Launcher App'

cd ~/Documents/projects/osx-packaging-scripts-master

python3 ~/Documents/projects/osx-packaging-scripts-master/__main__.py --qt-path /Users/noire/app/qt/5.11.2/clang_64 > build.log 2>&1

echo '==> Copying mcpelauncher-extract binary to the Minecraft Bedrock Launcher App'

cp -v ~/Documents/projects/osx-packaging-scripts-master/mcpelauncher-extract ~/Documents/projects/osx-packaging-scripts-master/output/Minecraft\ Bedrock\ Launcher.app/Contents/MacOS

echo '==> Codesigning Minecraft Bedrock Launcher App'

codesign -vvvv --force --deep -s "Mac Developer: 0ajwalker@gmail.com" ~/Documents/projects/osx-packaging-scripts-master/output/Minecraft\ Bedrock\ Launcher.app

echo '==> Creating DMG'

python3 ~/Documents/projects/osx-packaging-scripts-master/build_dmg.py

echo '==> Codesigning DMG'

codesign -v --force --deep -s "Mac Developer: 0ajwalker@gmail.com" ~/Documents/projects/osx-packaging-scripts-master/output/Minecraft\ Bedrock\ Launcher.dmg

echo '==> Compressing DMG to tar.xz'

cd output

tar -vcJf Minecraft\ Bedrock\ Launcher.dmg.tar.xz Minecraft\ Bedrock\ Launcher.dmg

cd ..

echo '==> Finished Build'

date
