#!/bin/bash
echo "patching AppleHDA.kext info and version .plists"
cp -v ~/Desktop/AppleHDA.kext\ 2/Contents/Info.plist ~/Desktop/AppleHDA.kext/Contents/Info.plist
cp -v ~/Desktop/AppleHDA.kext\ 2/Contents/version.plist ~/Desktop/AppleHDA.kext/Contents/version.plist
cp -v ~/Desktop/AppleHDA.kext\ 2/Contents/PlugIns/AppleHDAController.kext/Contents/Info.plist ~/Desktop/AppleHDA.kext/Contents/PlugIns/AppleHDAController.kext/Contents/Info.plist
cp -v ~Desktop/AppleHDA.kext\ 2/Contents/PlugIns/AppleHDAController.kext/Contents/version.plist ~Desktop/AppleHDA.kext/Contents/PlugIns/AppleHDAController.kext/Contents/version.plist
cp -v ~/Desktop/AppleHDA.kext\ 2/Contents/PlugIns/AppleHDAHALPlugIn.bundle/Contents/Info.plist ~/Desktop/AppleHDA.kext/Contents/PlugIns/AppleHDAHALPlugIn.bundle/Contents/Info.plist
cp -v ~/Desktop/AppleHDA.kext\ 2/Contents/PlugIns/AppleHDAHALPlugIn.bundle/Contents/version.plist ~/Desktop/AppleHDA.kext/Contents/PlugIns/AppleHDAHALPlugIn.bundle/Contents/version.plist
cp -v ~/Desktop/AppleHDA.kext\ 2/Contents/PlugIns/AppleHDAHardwareConfigDriver.kext/Contents/Info.plist ~/Desktop/AppleHDA.kext/Contents/PlugIns/AppleHDAHardwareConfigDriver.kext/Contents/Info.plist
cp -v ~/Desktop/AppleHDA.kext\ 2/Contents/PlugIns/AppleHDAHardwareConfigDriver.kext/Contents/version.plist ~/Desktop/AppleHDA.kext/Contents/PlugIns/AppleHDAHardwareConfigDriver.kext/Contents/version.plist
cp -v ~/Desktop/AppleHDA.kext\ 2/Contents/PlugIns/AppleMikeyDriver.kext/Contents/Info.plist ~/Desktop/AppleHDA.kext/Contents/PlugIns/AppleMikeyDriver.kext/Contents/Info.plist
cp -v ~/Desktop/AppleHDA.kext\ 2/Contents/PlugIns/AppleMikeyDriver.kext/Contents/version.plist ~/Desktop/AppleHDA.kext/Contents/PlugIns/AppleMikeyDriver.kext/Contents/version.plist
cp -v ~/Desktop/AppleHDA.kext\ 2/Contents/PlugIns/DspFuncLib.kext/Contents/Info.plist ~/Desktop/AppleHDA.kext/Contents/PlugIns/DspFuncLib.kext/Contents/Info.plist
cp -v ~/Desktop/AppleHDA.kext\ 2/Contents/PlugIns/DspFuncLib.kext/Contents/version.plist ~/Desktop/AppleHDA.kext/Contents/PlugIns/DspFuncLib.kext/Contents/version.plist
cp -v ~/Desktop/AppleHDA.kext\ 2/Contents/PlugIns/IOHDAFamily.kext/Contents/Info.plist ~/Desktop/AppleHDA.kext/Contents/PlugIns/IOHDAFamily.kext/Contents/Info.plist
cp -v ~/Desktop/AppleHDA.kext\ 2/Contents/PlugIns/IOHDAFamily.kext/Contents/version.plist ~/Desktop/AppleHDA.kext/Contents/PlugIns/IOHDAFamily.kext/Contents/version.plist
echo "patching Compleate please install with kext wizard or kext utility"
